$(document).ready(function () {
  $("#todoForm").submit(function (event) {
    event.preventDefault();
    let newTodo = $("#todoInput").val();
    let date = new Date().toLocaleDateString();
    $("#todoList").append(
      '<li class="list-group-item"><div class="form-check"><input type="checkbox" class="form-check-input"/><label class="check-label">' +
        newTodo +
        '</label></div><div class="date">' +
        "created:" +
        date +
        '</div><div class="button-container"><i class="fas fa-pencil-alt edit-button"></i><i class="remove-button fas fa-trash"></i></div></li>'
    );
    $("#todoInput").val("");
  });

  $("#todoList").on("click", ".remove-button", function () {
    $(this).closest("li").remove();
  });

  $("#todoList").on("click", ".edit-button", function () {
    let label = $(this).closest("li").find(".check-label");
    let date = $(this).closest("li").find(".date");
    let text = label.text().trim();
    label.replaceWith(
      '<input type="text" class="edit-input" value="' + text + '"/>'
    );
    date.text("Edited: " + new Date().toLocaleString());
  });

  $("#todoList").on("keypress", ".edit-input", function (event) {
    if (event.which === 13) {
      let newText = $(this).val().trim();
      let label = $('<label class="check-label"/>').text(newText);
      $(this).replaceWith(label);
      let date = $(this).closest("li").find(".date");
      date.text("Edited: " + new Date().toLocaleString());
    }
  });
});
